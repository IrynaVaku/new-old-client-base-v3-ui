///<reference types="cypress" />;
import SignUpPage from '../../support/page_object_model/signUpPage';
const signUpPage = new SignUpPage();
const testEmail = signUpPage.generateEmail();

describe('Sign Up main test suit', () => {
  before('preconditions -visit home page to sign up', () => {
    cy.visit('/user/register');
  });
  it('Page Title exists on the page', () => {
    cy.location('pathname').should('eq', '/v3/user/register');
  });
  it('Page Title exists on the page', () => {
    cy.title().should('include', '');
  });
  it('Page Header exists on the page', () => {
    signUpPage.headerSignUp().should('be.visible');
  });
  it('Page Header should have text', () => {
    signUpPage.headerSignUp().should('have.text', 'Create an account');
  });
  it('First Name field', () => {
    signUpPage
      .firstNameField()
      .should('be.visible')
      .should('have.attr', 'placeholder', 'First Name')
      .type('Ann')
      .should('have.value', 'Ann');
  });
  it('Last Name field', () => {
    signUpPage
      .lastNameField()
      .should('be.visible')
      .should('have.attr', 'placeholder', 'Last Name')
      .type('Ank')
      .should('have.value', 'Ank');
  });
  it('Email field', () => {
    signUpPage
      .emailField()
      .should('be.visible')
      .should('have.attr', 'placeholder', 'Email')
      .type(testEmail)
      .should('have.value', testEmail);
  });
  it('Password field', () => {
    signUpPage
      .passwordField()
      .should('be.visible')
      .should('have.attr', 'placeholder', 'Password')
      .type('132123')
      .should('have.value', '132123');
  });
  it('Submit Button', () => {
    signUpPage
      .submitBtn()
      .should('be.visible')
      .should('be.enabled')
      .should('have.text', 'Register')
      .click();
  });
  it('User was redirected on the onboarding page', () => {
    cy.location('pathname').should('eq', '/v3/onboarding');
  });
  it('Header on the onboarding page', () => {
    signUpPage
      .headerAfterSignUp()
      .should('be.visible')
      .should('have.text', `We sent you confirmation email to ${testEmail}.`)
      .contains('We sent you confirmation email to');
  });
});
